<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExcerptAddImageAddDateAddStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('food_recipes', function (Blueprint $table) {
            $table->text('excerpt')->nullable();
            $table->string('image')->nullable();
            $table->dateTime('date')->nullable();
            $table->boolean('status')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('food_recipes', function (Blueprint $table) {
            //
        });
    }
}
