@extends('pages.layout')

@section('title','Strona edytowania przepisu')


@section('content')

    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">Edycja przepisu</div>

                    <div class="card-body">
                        <form action="{{ route('pages.edit.recipe', ['recipe' => $recipe->id]) }}" method="post">
                            @csrf

                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label text-md-right">Tytuł przepisu</label>

                                <div class="col-md-6">
                                    <input id="name" type="text"
                                           class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                           name="name" value="{{ old('name', $recipe->name) }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="excerpt" class="col-md-4 col-form-label text-md-right">Wstęp</label>

                                <div class="col-md-6">
                                    <textarea name="excerpt" id="excerpt" cols="30" rows="2"
                                              class="form-control{{ $errors->has('excerpt') ? ' is-invalid' : '' }}"
                                              required>{{ old('excerpt', $recipe->excerpt) }}</textarea>
                                    @if ($errors->has('excerpt'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('excerpt') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="description" class="col-md-4 col-form-label text-md-right">Opis</label>

                                <div class="col-md-6">
                                    <textarea name="description" id="description" cols="30" rows="6"
                                              class="form-control{{ $errors->has('excerpt') ? ' is-invalid' : '' }}"
                                              required>{{ old('description', $recipe->description) }}</textarea>
                                    @if ($errors->has('description'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="status" class="col-sm-4 col-form-label text-md-right">
                                    Status
                                </label>
                                <div class="col-md-6">
                                    <select name="status" class="form-control">
                                        <option value="0">Nieaktywny</option>
                                        <option value="1" {{ old('status', $recipe->status) == 1 ? 'selected' : ''}}>
                                            Aktywny
                                        </option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="status" class="col-sm-4 col-form-label text-md-right">
                                    Kategoria
                                </label>
                                <div class="col-md-6">
                                    <select name="food_category_id" class="form-control">
                                        @foreach($categories as $category)
                                            <option
                                                value="{{ $category->id }}" {{ old('food_category_id', $recipe->food_category_id) == $category->id ? 'selected' : ''}}>
                                                {{ $category->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row mb-4">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        Edytuj przepis
                                    </button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
