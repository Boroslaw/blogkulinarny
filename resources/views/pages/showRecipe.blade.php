@extends('pages.layout')

@section('title','Przepisy')

@section('content')
    <div class="container mt-4">
        <div class="row">
            <div class="col-12">
                @if($recipe->food_category_id)

                    <h1>{{ $recipe->name }}</h1>
                    @if($recipe->category)
                        <a href="{{ route('pages.show.category', ['category' => $recipe->category->id]) }}">{{ $recipe->category->name }}</a>
                    @endif
                    @if($recipe->date)
                        <p>{{ $recipe->created_at->format('Y-m-d H:i') }}</p>
                    @endif

                    <p class="mt-3">{{ $recipe->description }}</p>

                    @if(Auth::check())
                        <div class="mt-5">
                            <a href="{{ route('pages.edit.recipe', ['recipe' => $recipe->id] )}}"
                               class="btn btn-primary">Edytuj
                                przepis</a>
                            <a href="{{ route('pages.delete.recipe', ['recipe' => $recipe->id] )}}"
                               class="btn btn-danger"
                               onclick="event.preventDefault();
                                             document.getElementById('delete-form').submit();">
                                Usuń przepis
                            </a>
                            <form id="delete-form"
                                  action="{{ route('pages.delete.recipe', ['recipe' => $recipe->id] )  }}"
                                  method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    @endif
                @endif
            </div>
        </div>
    </div>
@endsection
