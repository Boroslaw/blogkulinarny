@extends('pages.layout')
@section('title','Kategorie przepisów')

@section('content')

    <div class="container mt-4">
        <h1 class="mb-5">{{ $category->name }}</h1>
        @foreach($category->recipes as $recipe)
            <div class="row">
                <div class="col-12">
                    <h5>
                        <a href="{{ route('pages.show.recipe', ['recipe' => $recipe]) }}"
                           class="text-dark">{{ $recipe->name}}</a>
                        <small>
                            @if($recipe->category)
                                <a href="{{ route('pages.show.category', ['category' => $recipe->category->id]) }}">{{ $recipe->category->name }}</a>
                            @endif
                        </small>
                    </h5>
                    <p class="mb-0">{{ $recipe->excerpt }}</p>
                    <hr>
                </div>
            </div>
        @endforeach
    </div>

@endsection
