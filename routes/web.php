<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::auth();
Route::get('/','PagesController@home')->name('pages.home');
Route::get('/dodaj-przepis', 'PagesController@createRecipe')->name('pages.recipe.create')->middleware('auth');
Route::post('/dodaj-przepis', 'PagesController@postCreateRecipe')->name('pages.recipe.create')->middleware('auth');
Route::get('/przepisy/{recipe}', 'PagesController@showRecipe')->name('pages.show.recipe');
Route::get('/kategorie/{category}', "PagesController@showCategory")->name('pages.show.category');
Route::get('/przepisy/{recipe}/edytuj', 'PagesController@editRecipe')->name('pages.edit.recipe')->middleware('auth');
Route::post('przepisy/{recipe}/edytuj', 'PagesController@postEditRecipe')->name('pages.edit.recipe')->middleware('auth');
Route::post('przepisy/{recipe}/usun', 'PagesController@deleteRecipe')->name('pages.delete.recipe')->middleware('auth');
