<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FoodRecipe extends Model
{
    protected $fillable = [
        'name', 'description', 'food_category_id', "excerpt", "image", "date", "status"
    ];

    protected $dates = ['date'];


    public function category()
    {
        return $this->belongsTo(FoodCategory::class, 'food_category_id', 'id');
    }

    public function scopeActive($query)
    {
        return $query->where('status', true);
    }

    public function scopeOrdered($query)
    {
        return $query->orderBy('created_at','desc');
    }

}
