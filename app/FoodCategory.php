<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FoodCategory extends Model
{
    protected $fillable=[
      'name', 'order', 'status'
    ];

    public function recipes()
    {
        return $this->hasMany(FoodRecipe::class,'food_category_id','id');
    }

    public function scopeOrdered($query)
    {
        return $query->orderBy('order');
    }

    public function scopeActive($query)
    {
        return $query->where('status',true);
    }
}
