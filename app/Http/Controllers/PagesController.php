<?php

namespace App\Http\Controllers;

use App\FoodCategory;
use App\FoodRecipe;
use App\Http\Requests\RecipeRequest;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function home()
    {
        $recipes = FoodRecipe::ordered()->active()->get();
        $categories = FoodCategory::ordered()->active()->get();
        return view('pages.home', [
            'categories' => $categories,
            'recipes' => $recipes
        ]);
    }

    public function createRecipe()
    {
        $categories = FoodCategory::ordered()->active()->get();
        return view('pages.createRecipe', [
            'categories' => $categories
        ]);
    }

    public function postCreateRecipe(RecipeRequest $request)
    {
        $recipe = FoodRecipe::create($request->all());

        return redirect()->route('pages.show.recipe',['recipe' => $recipe->id]);
    }

    public function showRecipe(FoodRecipe $recipe)
    {
        $categories = FoodCategory::ordered()->active()->get();
        return view('pages.showRecipe',[
            'recipe' => $recipe,
            'categories' => $categories
        ]);
    }

    public function showCategory(FoodCategory $category)
    {
        $categories = FoodCategory::ordered()->active()->get();
        return view('pages.showCategory',[
           'categories'=>$categories,
           'category'=>$category
        ]);
    }

    public function editRecipe(FoodRecipe $recipe)
    {
        $categories = FoodCategory::ordered()->active()->get();
        return view('pages.editRecipe',[
            'categories' => $categories,
            'recipe' => $recipe
        ]);
    }

    public function postEditRecipe(RecipeRequest $request, FoodRecipe $recipe)
    {
        $recipe->update($request->all());
        return redirect()->back();
    }

    public function deleteRecipe(FoodRecipe $recipe)
    {
        $recipe->delete();
        return redirect()->route('pages.home');
    }
}
